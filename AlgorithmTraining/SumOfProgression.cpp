// ������ �� 1024
#include <iostream>
#include <vector>

int main()
{
	int sum, length;
	std::cin >> sum >> length;
	if (length < 2 || length > 100) return 0;

	std::vector<int> intVec(length);

	for (; length < 100; length++)
	{
		for (auto i = 0; i < length; i++)
		{
			intVec[i] = i;
		}

		while (true)
		{
			int sumOfElems = 0;

			for (auto &n : intVec)
			{
				sumOfElems += n;
			}

			if (sumOfElems == sum)
			{
				for (auto print=0;print<intVec.size()-1;print++)
				{
					std::cout << intVec[print] << " ";
				}
				std::cout << intVec.back();
				//system("pause");
				return 0;
			}

			if (intVec.back() == 99)
			{
				break;
			}

			for (auto x=0;x<intVec.size();x++)
			{
				intVec[x]++;				
			}			
		}

		intVec.resize(intVec.size() + 1);
	}

	std::cout << "-1";
	return 0;
}