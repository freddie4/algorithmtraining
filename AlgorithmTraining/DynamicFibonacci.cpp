// 동적 계획법 - 피보나치

#include <iostream>
#include <array>

int num_zero, num_one = 0;
std::array<int, 1000> fArray;

int Fibonacci(int n)
{
	if (n == 0)
	{
		num_zero++;
		return 0;
	}
	else if (n == 1)
	{
		num_one++;
		return 1;
	}
	else
	{
		fArray[0] = 0;
		fArray[1] = 1;
		return fArray[n] = Fibonacci(n - 1) + Fibonacci(n - 2);
	}
}

int main()
{
	int count;
	std::cin >> count;
	for (int i = 1; i <= count; i++)
	{
		int n;
		std::cin >> n;

		Fibonacci(n);
		std::cout << num_zero << " " << num_one << '\n';
		num_zero = 0;
		num_one = 0;
	}
	::getchar();
	return 0;
}