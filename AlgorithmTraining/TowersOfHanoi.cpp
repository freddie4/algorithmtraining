// 하노이의 탑
// 재귀보다 반복문이 빠르다
#include <iostream>

void TowerOfHanoi(const int numberOfStone, const char from, const char by, const char to)
{
	if (numberOfStone == 1)
	{
		std::cout << "1 원반을 " << from << "에서 " << to << "으로 이동" << '\n';
	}
	else
	{
		TowerOfHanoi(numberOfStone - 1, from, to, by);
		std::cout << numberOfStone << " 원반을 " << from << "에서 " << to << "로 이동" << '\n';
		TowerOfHanoi(numberOfStone - 1, by, from, to);
	}
}

int main()
{
	TowerOfHanoi(5, 'A', 'B', 'C');

	getchar();
	return 0;
}
