// 10220 Self Representing Seq

#include <iostream>

// n = 4, 5일 때는 유일해를 갖는다. (2개, 1개)
// n = 6일때는 해가 존재하지 않는다.
// n = 7부터 규칙을 갖지만 해는 1개이다.

int main()
{
	int testCase;
	std::cin >> testCase;
	while(testCase--)
	{
		int n;
		std::cin >> n;
		if (n < 4 || n == 6) std::cout << 0 << '\n';
		else if (n == 4) std::cout << 2 << '\n';
		else if (n == 5) std::cout << 1 << '\n';
		else std::cout << 1 << '\n';
	}
	return 0;
}