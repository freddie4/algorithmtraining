// 지능형 기차

#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
	int currentPeople = 0;
	std::pair<int, int> pair;
	std::vector<int> peopleNumVec;

	for (auto i = 0; i < 4; i++)
	{
		std::cin >> pair.first >> pair.second;
		currentPeople -= pair.first;
		peopleNumVec.push_back(currentPeople);
		currentPeople += pair.second;
		peopleNumVec.push_back(currentPeople);
	}

	const auto maxNum = *std::max_element(peopleNumVec.begin(), peopleNumVec.end());
	std::cout << maxNum;

	system("pause");
	return 0;
}