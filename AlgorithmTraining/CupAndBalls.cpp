// 공 1547

#include <algorithm>
#include <iostream>
#include <vector>

void Swap(std::vector<int>& vec, const int x, const int y)
{
	const auto xpos = std::distance(vec.begin(), std::find(vec.begin(), vec.end(), x));
	const auto ypos = std::distance(vec.begin(), std::find(vec.begin(), vec.end(), y));

	const auto temp = vec[xpos];
	vec[xpos] = vec[ypos];
	vec[ypos] = temp;
}

int main()
{
	int m;
	//std::cout << "컴의 위치를 바꿀 횟수(<=50) : ";
	std::cin >> m;

	int cupX, cupY;
	std::vector<int> cupVec = { 1,2,3 };

	while(m--)
	{
		//std::cout << "컴의 위치를 바꿀 방법 : ";
		std::cin >> cupX >> cupY;

		Swap(cupVec, cupX, cupY);
	}

	std::cout << cupVec[0];

	//::system("pause");
	return 0;
}

// vector 안쓰고 지역변수 교환으로만 해결하도록 바꾸자