// � 10253

#include <iostream>

int GCD(int a, int b)
{
	return b == 0 ? a : GCD(b, a%b);
}

int Function(int _a, int _b)
{
	int x;

	int a = _a;
	int b = _b;

	while (a != 1)
	{
		x = (b%a == 0) ? (b / a) : (b / a + 1);
		a = a*x - b;
		b *= x;

		int g = GCD(a, b);
		a /= g;
		b /= g;
	}

	return b;
}

int main()
{
	int testData;
	std::cin >> testData;

	for (auto i = 0; i < testData; i++)
	{
		int a, b;
		std::cin >> a >> b;
		std::cout << Function(a, b) << '\n';
	}

	::getchar();
	return 0;
}