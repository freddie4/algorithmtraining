// RGB거리

#include <algorithm>
#include <iostream>
#include <vector>

enum class RGB
{
	R = 0,
	G = 1,
	B = 2
};

int main()
{
	int houseNum;

	std::cout << "집 수 입력 : ";
	std::cin >> houseNum;
	std::vector<std::vector<int>> colorVec(houseNum, std::vector<int>(3, -1));
	std::vector<int> distanceVec(3);

	for (auto i = 0; i < houseNum; i++)
	{
		std::cout << "비용입력(red, green, blue) : ";
		std::cin >> colorVec[i][0] >> colorVec[i][1] >> colorVec[i][2];
	}

	for (auto i = 0; i < colorVec.size(); i++)
	{
		for (auto j = 0; j < colorVec[i].size(); j++)
		{
			std::cout << colorVec[i][j] << " ";
		}
		std::cout << '\n';
	}

	for (auto i = 1; i <= houseNum; i++)
	{
		distanceVec[0] = std::min(distanceVec[1], distanceVec[2]) + colorVec[i][0];
		distanceVec[1] = std::min(distanceVec[0], distanceVec[2]) + colorVec[i][1];
		distanceVec[2] = std::min(distanceVec[0], distanceVec[1]) + colorVec[i][2];
	}

	std::cout << std::min(std::min(distanceVec[houseNum][0], distanceVec[houseNum][1]), distanceVec[houseNum][2]);
	
	::system("pause");
	::getchar();
	return 0;
}