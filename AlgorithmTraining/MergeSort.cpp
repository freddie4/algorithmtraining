// insertion Sort time complexity : O(n^2)
// double for loop

#include <vector>
#include <iostream>

void Print(const std::vector<int>& array)
{
	for (const auto& i : array)
	{
		std::cout << i << " ";
	}

	std::cout << '\n';
}

void MergeSort(std::vector<int>& array, const int start, const int end)
{
	const auto arraySize = static_cast<int>(array.size());
}

int main()
{
	std::vector<int> vec = { 4,1,7,3,5,9,8,10,2,6 };

	Print(vec);

	MergeSort(vec, 0, vec.size() - 1);

	Print(vec);

	::system("pause");
	return 0;
}