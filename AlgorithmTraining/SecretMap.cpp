// 카카오 1차 1번 비밀지도
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <bitset>

void Function(const int n, std::vector<int>& arr1, std::vector<int>& arr2)
{
	std::vector<std::string> resultVec;

	for(auto i = 0; i < n; ++i)
	{
		const auto binary = [=](const int dec) -> std::string
		{
			auto bin = std::bitset<5>(dec).to_string();

			std::replace(bin.begin(), bin.end(), '1', '#');
			std::replace(bin.begin(), bin.end(), '0', ' ');

			return bin;
		}(arr1[i] | arr2[i]);

		resultVec.push_back(binary);
	}
}

int main()
{
	std::cout << "input n : ";
	std::cout << '\n';

	auto n = 0;
	std::cin >> n;

	std::vector<int> arr1;
	std::vector<int> arr2;

	std::cout << "input arr1 * n : ";
	std::cout << '\n';
	for(auto i = 0; i < n; ++i)
	{
		auto _n = 0;
		std::cin >> _n;
		arr1.push_back(_n);
	}

	std::cout << "input arr2 * n : ";
	std::cout << '\n';
	for (auto i = 0; i < n; ++i)
	{
		auto _n = 0;
		std::cin >> _n;
		arr2.push_back(_n);
	}

	Function(n, arr1, arr2);

	::system("pause");
	return 0;
}