// 카카오 1-3 LRU 캐시
// cache hit : 1 sec, cache miss : 5 sec
#include <iostream>
#include <string>
#include <vector>
#include <list>

int main()
{
	constexpr auto CACHE_HIT = 1;
	constexpr auto CACHE_MISS = 5;
	constexpr auto CACHE_SIZE = 3;

	auto elapsedTime = 0;

	std::vector<std::string> cityVec = { "Jeju", "Pangyo", "Seoul", "NewYork", "LA",
		"Jeju", "Pangyo", "Seoul", "NewYork", "LA" };

	if(CACHE_SIZE != 0)
	{
		std::list<std::string> cache;

		for (auto i = 0; i < CACHE_SIZE; ++i)
		{
			cache.push_back(cityVec[i]);
			elapsedTime += CACHE_MISS;
		}

		for (auto i = CACHE_SIZE; i < cityVec.size(); ++i)
		{
			const auto found = std::find(cache.begin(), cache.end(), cityVec[i]);
			if (found == cache.end())
			{
				cache.pop_front();
				cache.push_back(cityVec[i]);
				elapsedTime += CACHE_MISS;
			}
			else
			{
				cache.erase(found);
				cache.push_back(cityVec[i]);
				elapsedTime += CACHE_HIT;
			}
		}
	}
	else
	{
		elapsedTime = CACHE_MISS * cityVec.size();
	}

	std::cout << elapsedTime << '\n';

	::system("pause");
	return 0;
}
