// 10216 Count Circle Groups

#include <iostream>
#include <vector>
#include <tuple>

using Camp = std::tuple<int, int, int>;
Camp c[3001];
std::vector<int> adj[3001];
int n;

bool IsGroup(Camp camp1, Camp camp2)
{
	const auto distance = (std::get<0>(camp1) - std::get<0>(camp2))*(std::get<0>(camp1) - std::get<0>(camp2))
		+ (std::get<1>(camp1) - std::get<1>(camp2))*(std::get<1>(camp1) - std::get<1>(camp2));
	return distance <= (std::get<2>(camp1) + std::get<2>(camp2))*(std::get<2>(camp1) + std::get<2>(camp2));
}

int CountGroup()
{
	int group = 0;

	bool visited[3001] = { false };

	for(auto i = 0; i < n; i++)
	{
		for(auto j = 0; j < n; j++)
		{
			if(IsGroup(c[i], c[j]))
			{
				adj[i].push_back(j);
				adj[j].push_back(i);
			}
		}
	}

	// 탐색
	std::vector<int> vec;
	for(auto i = 0; i < n; i++)
	{
		if (visited[i]) continue;
		visited[i] = true;
		vec.push_back(i);
		while(!vec.empty())
		{
			const auto cur = vec.back();
			vec.pop_back();

			for(auto j = 0; j < adj[cur].size(); j++)
			{
				auto temp = adj[cur][j];
				if(!visited[temp])
				{
					visited[temp] = true;
					vec.push_back(temp);
				}
			}
		}
		group++;
	}

	return group;
}

int main()
{
	int testCase;
	std::cin >> testCase;
	while(testCase--)
	{
		//std::cout << "적군 진영의 숫자 N : " << '\n';
		std::cin >> n;
		
		for(auto i = 0; i < n; i++)
		{
			adj[i].clear();
			int x, y, r;
			std::cin >> x >> y >> r;
			c[i] = std::tuple<int, int, int>(x, y, r);
		}

		std::cout << CountGroup() << '\n';
	}

	return 0;
}