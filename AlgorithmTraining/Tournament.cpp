// 토너먼트 1057
// 쓸데없는 코드 줄일 수 있음

#include <iostream>
#include <vector>

int main()
{
	int totalPlayers, kim, han;
	int rounds = 1;
	std::cin >> totalPlayers >> kim >> han;

	std::vector<int> playerVec;
	for (auto i = 1; i <= totalPlayers; i++)
	{
		playerVec.push_back(i);
	}

	while (true)
	{
		if (((kim % 2 == 1) && kim + 1 == han) || ((han % 2 == 1) && han + 1 == kim))
		{
			break;
		}

		if (playerVec.size() % 2 == 1)
		{
			playerVec.clear();
			for (auto i = 1; i <= (playerVec.size() + 1) / 2; i++)
			{
				playerVec.push_back(i);
			}

			if (kim % 2 == 1)
			{
				kim = (kim + 1) / 2;
			}
			else
			{
				kim = kim / 2;
			}

			if (han % 2 == 1)
			{
				han = (han + 1) / 2;
			}
			else
			{
				han = han / 2;
			}
		}
		else
		{
			playerVec.clear();
			for (auto i = 1; i <= playerVec.size() / 2; i++)
			{
				playerVec.push_back(i);
			}

			if (kim % 2 == 1)
			{
				kim = (kim + 1) / 2;
			}
			else
			{
				kim = kim / 2;
			}

			if (han % 2 == 1)
			{
				han = (han + 1) / 2;
			}
			else
			{
				han = han / 2;
			}
		}

		rounds++;
	}

	std::cout << rounds;
	system("pause");
	return 0;
}