// kakao 1-6 
#include <iostream>
#include <string>
#include <vector>

void Print(std::vector<std::vector<std::string>>& vec)
{
	for(const auto& i : vec)
	{
		for(const auto& j : i)
		{
			std::cout << j << " ";
		}
		std::cout << '\n';
	}
}

void Run(std::vector<std::vector<std::string>>& vec, const int m, const int n)
{
	std::vector<std::pair<int, int>> removeVec;

	for(auto i = 0; i < m; ++i)
	{
		for(auto j = 0; j < n; ++j)
		{
			// have to remove?
			const auto value = vec[i][j];
			if(value == vec[i-1][j-1] && value == vec[i-1][j] && value == vec[i][j-1])
			{
				
			}
		}
	}
}

int main()
{
	std::vector<std::vector<std::string>> inputVec =
	{
		std::vector<std::string>({ "C","C","B","D","E" }),
		std::vector<std::string>({ "A","A","A","D","E" }),
		std::vector<std::string>({ "A","A","A","B","F" }),
		std::vector<std::string>({ "C","C","B","B","F" })
	};

	Print(inputVec);

	Run(inputVec, 4, 5);

	Print(inputVec);

	return 0;
}