// 10214 Baseball

#include <iostream>

int main()
{
	int testCase;
	//std::cout << "testCase : " << '\n';
	std::cin >> testCase;

	while (testCase--)
	{
		int yScore = 0, kScore = 0;

		for (auto i = 0; i < 9; i++)
		{
			int y, k;
			//std::cout << i << "��° ���� Y, K �Է� : " << '\n';
			std::cin >> y >> k;
			yScore += y;
			kScore += k;
		}

		const auto result = yScore == kScore ? "Draw" : yScore > kScore ? "Yonsei" : "Korea";
		std::cout << result << '\n';
	}

	//::system("pause");
	return 0;
}