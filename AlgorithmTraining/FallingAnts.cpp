// 떨어지는 개미 3163

#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>
#include <tuple>

using namespace std;

// 개미를 움직여가며 시뮬레이션하면 시간초과가 뜬다. 
// 개미의 처음 위치를 기반으로 움직일 거리를 계산해서 순위를 정해보자.

int main() {
	int T, N, L, K;
	int pos, id;
	vector<pair<int, int> > ant;
	deque<int> antId;
	vector<pair<int, int> > res;
	scanf("%d", &T);

	while (T > 0) {
		ant.clear();
		antId.clear();
		scanf("%d %d %d", &N, &L, &K);
		ant = vector<pair<int, int> >(N);
		res = vector<pair<int, int> >(N);

		for (int i = 0; i<N; i++) {
			scanf("%d %d", &pos, &id);
			if (id > 0) {
				pos = L - pos;
			}
			ant[i].first = pos;
			ant[i].second = id;
			antId.push_back(ant[i].second);
		}
		sort(ant.begin(), ant.end());

		for (int i = 0; i<N; i++) {
			res[i].first = ant[i].first;
			if (ant[i].second > 0) {
				res[i].second = antId.back();
				antId.pop_back();
			}
			else {
				res[i].second = antId.front();
				antId.pop_front();
			}
		}

		sort(res.begin(), res.end());
		printf("%d\n", res[K - 1].second);
		T--;
	}
}
