// 카카오 1-2 다트 게임
#include <iostream>
#include <string>
#include <vector>
#include <regex>

enum class Square
{
	Single = 1,
	Double = 2,
	Triple = 3,
};

enum class Option
{
	Star,
	Acha,
	None
};

Square StringToEnumSquare(const std::string& str)
{
	if (str == "S") return Square::Single;
	if (str == "D") return Square::Double;
	if (str == "T") return Square::Triple;
	std::runtime_error("Invalid Input String");
}

Option StringToEnumOption(const std::string& str)
{
	if (str == "*") return Option::Star;
	if (str == "#") return Option::Acha;
	if (str.empty()) return Option::None;
	std::runtime_error("Invalid Input String");
}

int Function(const std::string& dartResult)
{
	const std::regex regex(R"((\d+)([SDT])([#*]?)(\d+)([SDT])([#*]?)(\d+)([SDT])([#*]?))");
	std::smatch match;
	
	std::vector<int> totalVec(3);
	auto totalScore = 0;

	if (std::regex_search(dartResult, match, regex))
	{
		auto k = 1;
		for (auto i = 1; i <= 3; i++)
		{
			auto score = std::stoi(match[k].str());
			const auto square = match[k + 1].str();
			const auto option = match[k + 2].str();

			switch (StringToEnumSquare(square))
			{
			case Square::Single:
			{
				// Do Nothing
				break;
			}
			case Square::Double:
			{
				score = (score * score);
				break;
			}
			case Square::Triple:
			{
				score = (score * score * score);
				break;
			}
			}

			switch (StringToEnumOption(option))
			{
			case Option::Star:
			{
				if (i > 1)
				{
					totalVec[i - 2] *= 2;
				}
				score *= 2;
				break;
			}
			case Option::Acha:
			{
				score *= -1;
				break;
			}
			case Option::None:
			{
				// Do Nothing
				break;
			}
			}

			totalVec[i - 1] = score;
			k = k + 3;
		}
	}

	for (auto& i : totalVec)
	{
		totalScore += i;
	}

	return totalScore;
}

int main()
{
	const std::string dartResult = "1S*2T*3S";

	std::cout << Function(dartResult) << '\n';

	::system("pause");
	return 0;
}