// quick Sort time complexity : O(nlogn) ~ O(n^2)
// divide and conquer

#include <vector>
#include <iostream>

void Print(const std::vector<int>& array)
{
	for (const auto& i : array)
	{
		std::cout << i << " ";
	}

	std::cout << '\n';
}

void QuickSort(std::vector<int>& array, const int leftIndex, const int rightIndex)
{
	int left = leftIndex;
	int right = rightIndex;

	const auto pivot = array[(left + right) / 2];

	while (left <= right)
	{
		while (array[left] < pivot)
		{
			left++;
		}
		while (array[right] > pivot)
		{
			right--;
		}

		if(left <= right)
		{
			const auto temp = array[left];
			array[left] = array[right];
			array[right] = temp;

			left++;
			right--;
		}
	}

	if(leftIndex < right)
	{
		QuickSort(array, leftIndex, right);
	}

	if(left < rightIndex)
	{
		QuickSort(array, left, rightIndex);
	}
}

int main()
{
	std::vector<int> vec = { 4,1,7,3,5,9,8,10,2,6 };

	Print(vec);

	QuickSort(vec, 0, vec.size() - 1);

	Print(vec);

	::system("pause");
	return 0;
}