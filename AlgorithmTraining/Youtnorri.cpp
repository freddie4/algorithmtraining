// ������

#include <iostream>
#include <vector>

int main()
{
	for (auto i = 0; i < 3; i++)
	{
		int a, b, c, d;
		int frontCount = 0;
		std::vector<int> intVec;

		std::cin >> a >> b >> c >> d;

		intVec.push_back(a);
		intVec.push_back(b);
		intVec.push_back(c);
		intVec.push_back(d);

		for (auto& i : intVec)
		{
			if (i == 1)
			{
				frontCount++;
			}
		}

		switch (frontCount)
		{
		case 0:
			std::cout << "D" << '\n';
			break;
		case 1:
			std::cout << "C" << '\n';
			break;
		case 2:
			std::cout << "B" << '\n';
			break;
		case 3:
			std::cout << "A" << '\n';
			break;
		case 4:
			std::cout << "E" << '\n';
			break;
		default:
			return 0;
		}
	}	

	getchar();
	return 0;
}