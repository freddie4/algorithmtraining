// ����� 1094

#include <algorithm>
#include <iostream>
#include <vector>

int VectorSum(std::vector<int> vec)
{
	int result = 0;
	for (auto& i : vec)
	{
		result += i;
	}
	return result;
}

int main()
{
	std::vector<int> stickVec;
	stickVec.push_back(64);

	int wanted_stick;
	std::cin >> wanted_stick;
	if (wanted_stick > 64) return 0;

	while (VectorSum(stickVec) > wanted_stick)
	{
		const auto min_stick = *std::min_element(stickVec.begin(), stickVec.end());
		const auto min_stick_iter = std::min_element(stickVec.begin(), stickVec.end());
		stickVec.erase(min_stick_iter);
		stickVec.push_back(min_stick / 2);

		if (VectorSum(stickVec) >= wanted_stick)
		{
			// drop
		}
		else
		{
			stickVec.push_back(min_stick / 2);
		}

		if (VectorSum(stickVec) == wanted_stick)
		{
			break;
		}
	}

	std::cout << stickVec.size();

	system("pause");
	return 0;
}