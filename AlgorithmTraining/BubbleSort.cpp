// Bubble Sort time complexity : O(n^2)
// double for loop

#include <string>
#include <vector>
#include <iostream>

void Print(const std::vector<int>& array)
{
	for(const auto& i : array)
	{
		std::cout << i << " ";
	}

	std::cout << '\n';
}

void BubbleSort(std::vector<int>& array)
{
	const auto arraySize = static_cast<int>(array.size());

	for(auto i = 0; i < arraySize; ++i)
	{
		for(auto j = i + 1; j < arraySize; ++j)
		{
			if(array[i] > array[j])
			{
				const auto temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}
}

int main()
{
	std::vector<int> vec = { 4,1,7,3,5,9,8,10,2,6 };

	Print(vec);

	BubbleSort(vec);

	Print(vec);

	::system("pause");
	return 0;
}