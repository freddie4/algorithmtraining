// 카카오 1-5 뉴스 클러스터링 - 자카드 유사도
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <locale>
#include <cctype>
#include <iterator>

void SubStr(const std::string& string, std::vector<std::string>& vec)
{
	// substr string
	for (auto i = 0; i < string.size(); ++i)
	{
		const auto subString = string.substr(i, 2);

		const auto isAlpha = std::find_if(subString.begin(), subString.end(),
			[](char c)
		{
			return !std::isalpha(c);
		}) == subString.end();

		if (true == isAlpha && subString.size() == 2)
		{
			vec.emplace_back(subString);
		}
	}
}

int main()
{
	std::string str1 = "FRANCE";
	std::string str2 = "FRENCH";

	std::vector<std::string> substrVec1;
	std::vector<std::string> substrVec2;

	SubStr(str1, substrVec1);
	SubStr(str2, substrVec2);

	std::sort(substrVec1.begin(), substrVec1.end());
	std::sort(substrVec2.begin(), substrVec2.end());

	std::vector<std::string> intersectionVec;
	std::vector<std::string> unionVec;

	std::set_intersection(substrVec1.begin(), substrVec1.end(),
		substrVec2.begin(), substrVec2.end(), std::back_inserter(intersectionVec));

	std::set_union(substrVec1.begin(), substrVec1.end(),
		substrVec2.begin(), substrVec2.end(), std::back_inserter(unionVec));
	
	const auto vec1size = static_cast<double>(intersectionVec.size());
	const auto vec2size = static_cast<double>(unionVec.size());

	// if size == 0, zacard = 1

	const auto zacard = vec1size / vec2size;

	std::cout << zacard * 65536;

	::system("pause");
	return 0;
}

