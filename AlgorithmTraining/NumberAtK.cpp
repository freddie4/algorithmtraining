// 11004 분할정복 K번째 수

#include <algorithm>
#include <iostream>
#include <vector>

// vector 대신 배열을 쓰면 시간이 덜걸리나
int main()
{
	std::vector<int> intVec;
	int n, k;
	std::cin >> n >> k;
	while(n--)
	{
		int input;
		std::cin >> input;
		intVec.push_back(input);
	}

	std::sort(intVec.begin(), intVec.end());

	std::cout << intVec[k - 1];

	::system("pause");
	return 0;
}
