// X보다 작은 수
#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
	int n, x;
	std::cin >> n >> x;

	std::vector<int> intVec(n);
	std::vector<int> resultVec;

	for (auto i = 0; i < n; i++)
	{
		std::cin >> intVec[i];
	}
	
	for (auto i : intVec)
	{
		if (i < x)
		{
			std::cout << i << " ";
		}
	}

	::getchar();
	return 0;
}