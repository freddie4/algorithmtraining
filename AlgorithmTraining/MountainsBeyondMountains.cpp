// 13119 Mountains Beyond Mountains

#include <iostream>
#include <string>
#include <vector>

void PrintVec(std::vector<std::vector<std::string>>& vec)
{
	for(const auto& i : vec)
	{
		for(const auto& j : i)
		{
			std::cout << j;
		}
		std::cout << '\n';
	}
}

void Draw(std::vector<int>& height, const int n, const int m, const int x)
{
	std::vector<std::vector<std::string>> printVec(n, std::vector<std::string>(m, "."));

	// draw mountain
	for(auto h = 0; h < m; ++h)
	{
		for(auto i = 0; i < height[h]; ++i)
		{
			printVec[n - i - 1][h] = "#";
		}
	}

	// draw highway, pier
	for(auto i = 0; i < m; ++i)
	{
		if(printVec[n - x][i] == "#")
		{
			printVec[n - x][i] = "*";
		}
		else
		{
			printVec[n - x][i] = "-";
			if((i + 1) % 3 == 0 && height[i] < x)
			{
				for(auto j = 0; j < x - 1 - height[i]; ++j)
				{
					printVec[x - 1 + j][i] = "|";
				}
			}
		}
	}

	PrintVec(printVec);
}

int main()
{
	int n = 0;
	int m = 0;
	int x = 0;
	std::cin >> n >> m >> x;

	std::vector<int> height(m);

	for(auto i = 0; i < m; ++i)
	{
		std::cin >> height[i];
	}

	Draw(height, n, m, x);

	return 0;
}