// insertion Sort time complexity : O(n^2)
// double for loop

#include <vector>
#include <iostream>

void Print(const std::vector<int>& array)
{
	for (const auto& i : array)
	{
		std::cout << i << " ";
	}

	std::cout << '\n';
}

void InsertionSort(std::vector<int>& array)
{
	const auto arraySize = static_cast<int>(array.size());
	
	for(auto i = 1; i < arraySize; ++i)
	{
		const auto temp = array[i];
		auto aux = i - 1;

		while(aux >= 0 && array[aux] > temp)
		{
			array[aux + 1] = array[aux];
			aux--;
		}

		array[aux + 1] = temp;
	}
}

int main()
{
	std::vector<int> vec = { 4,1,7,3,5,9,8,10,2,6 };

	Print(vec);

	InsertionSort(vec);

	Print(vec);

	::system("pause");
	return 0;
}