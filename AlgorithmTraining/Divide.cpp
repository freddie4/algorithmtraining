// 나눗셈
// 0 < A,B < 10
// 소수점 9자리까지 출력(체크!)

#include <iostream>
#include <string>

int main()
{
	double a, b;
	std::cin >> a >> b;
	if (a <= 0 || a >= 10 || b <= 0 || b >= 10)
	{
		return 0;
	}
	std::cout.precision(9);
	std::cout << a / b;

	::getchar();
	return 0;
}