// 입력받은 문자 그대로 출력(공백, 줄바꿈 포함, EOF: ctrl+z x3)

#include <iostream>
#include <string>
#include <vector>

int main()
{
	std::vector<std::string> inputVec;
	std::string inputStr;

	while (std::getline(std::cin, inputStr))
	{
		inputVec.push_back(inputStr);
	}

	for (auto i : inputVec)
	{
		std::cout << i << '\n';
	}

	::getchar();
	return 0;
}