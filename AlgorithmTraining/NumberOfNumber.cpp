// 숫자의 개수

#include <iostream>
#include <string>

int main()
{
	int number1, number2, number3;

	std::cin >> number1;
	std::cin >> number2;
	std::cin >> number3;

	std::string numbers = std::to_string(number1*number2*number3);
	
	std::cout << std::count(numbers.begin(), numbers.end(), '0') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '1') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '2') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '3') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '4') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '5') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '6') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '7') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '8') << '\n';
	std::cout << std::count(numbers.begin(), numbers.end(), '9') << '\n';

	system("pause");
	return 0;
}