// 13118 NewtonAndApple
// 문제가 좀 이상함 y, r은 왜있는거지

#include <iostream>
#include <string>
#include <vector>

void Function(const std::vector<int>& position, const int x, const int y, const int r)
{
	auto result = false;
	for(auto i = 0; i < position.size(); ++i)
	{
		if(position[i] == x)
		{
			std::cout << i + 1 << " ";
			if(!result) result = true;
		}
	}

	if(!result)
	{
		std::cout << 0 << '\n';		
	}
}

int main()
{
	std::vector<int> vec(4);

	for(auto i = 0; i < 4; ++i)
	{
		std::cin >> vec[i];
	}

	int x = 0;
	int y = 0;
	int r = 0;

	std::cin >> x >> y >> r;
	
	Function(vec, x, y, r);

	return 0;
}