// Meats On The Grill 10219

#include <iostream>
#include <string>
#include <vector>

void PrintGrill(const std::vector<std::vector<char>>& grill, const int height, const int width)
{
	for (auto i = 0; i < height; i++)
	{
		for (auto j = 0; j < width; j++)
		{
			std::cout << grill[i][j];
		}
		std::cout << '\n';
	}
}

int main()
{
	int testCase;
	// std::cout << "testcase input : ";
	std::cin >> testCase;

	while (testCase--)
	{
		int height, width;
		//std::cout << "h, w input : ";
		std::cin >> height >> width;

		std::vector<std::vector<char>> grill(height, std::vector<char>(width, '.'));
		auto reversedGrill = grill;

		//std::cout << "불판의 상태 입력(문자열)" << '\n';
		for (auto i = 0; i < height; i++)
		{
			std::string tempString;
			std::cin >> tempString;
			for(auto j = 0; j < width; j++)
			{
				grill[i][j] = tempString[j];
			}
		}

		//PrintGrill(grill, height, width);

		// 좌우 뒤집기(상하 뒤집기도 가능)
		for(auto i = 0; i < height; i++)
		{
			for ( auto j = 0; j < width; j++)
			{
				reversedGrill[i][j] = grill[i][width - j - 1];
			}
		}

		PrintGrill(reversedGrill, height, width);
	}

	//::system("pause");
	return 0;
}