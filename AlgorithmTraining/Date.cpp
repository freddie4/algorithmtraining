// 날짜 계산하기

#include <iostream>
#include <string>
#include <unordered_map>

int main()
{
	int a, b;
	std::cin >> a >> b;
	std::unordered_map<int, std::string> weekMap = { {0, "MON"}, {1, "TUE"}, {2, "WED"}, {3, "THU"}, {4, "FRI"}, {5, "SAT"}, {6, "SUN"} };

	int month[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

	int day = 0;
	int date = 0;

	for (int i = 0; i < a - 1; i++)
	{
		day += month[i];
	}

	day += b - 1;
	date = day % 7;

	std::cout << weekMap[date];

	::getchar();
	return 0;
}